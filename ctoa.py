#! /usr/bin/env python
# encoding: utf-8

import sys
import pysam
from Bio import SeqIO as seq

# cigar mapping table
MATCH  = 0  # M
INS    = 1  # I
DEL    = 2  # D
SKIP   = 3  # N
SOFT   = 4  # S
HARD   = 5  # H
PAD    = 6  # P
EQUAL  = 7  # =
DIFF   = 8  # X

# read type
FWD    = 1
REV    = 2
TD     = 4


def decode_cigar(cigar):
	
	s = []

	for p in cigar:
		if p[0] == MATCH or p[0] == EQUAL:
			s.extend(['M' for i in range(p[1])])
		elif p[0] == DIFF:
			s.extend(['X' for i in range(p[1])])
		elif p[0] == DEL:
			s.extend(['D' for i in range(p[1])])
		elif p[0] == INS:
			s.extend(['I' for i in range(p[1])])
		elif p[0] == SOFT:
			s.extend(['N' for i in range(p[1])])
	return ''.join(s)			

def correct_dcigar(aln, ref):

	dc = decode_cigar(aln.cigar)
	seq = aln.seq.decode('UTF-8')
	qi = 0
	ri = aln.pos

	s = []

	for c in dc:
		if c == 'M':
			if seq[qi] == ref[ri]:
				s.extend('M')
			else:
				s.extend('X')
		else:
			s.extend(c)

		if c == 'M' or c == 'X' or c == 'D':
			ri += 1

		if c == 'M' or c == 'X' or c == 'I' or c == 'N':
			qi += 1

	return ''.join(s)


def tostring(aln, ref):
	q = []
	s = []
	r = []

	qi = 0
	ri = aln.pos

	dc = correct_dcigar(aln, ref)
	seq = aln.seq.decode('UTF-8')
	for c in dc:
		if c == 'M' or c == 'X' or c == 'D':
			r.extend(ref[ri])
			ri += 1
		else:
			r.extend('-')

		if c == 'M' or c == 'X' or c == 'I' or c == 'N':
			q.extend(seq[qi])
			qi += 1
		else:
			q.extend('-')

		if c == 'M':
			s.extend('|')
		elif c == 'X':
			s.extend('x')
		else:
			s.extend(' ')

	astr = ''
	qstr = ''.join(q)
	cstr = ''.join(s)
	rstr = ''.join(r)

	fold = 200

	i = 0
	while i < len(cstr):
		astr += (rstr[i:min(i+fold, len(rstr))] + "\n")
		astr += (cstr[i:min(i+fold, len(cstr))] + "\n")
		astr += (qstr[i:min(i+fold, len(qstr))] + "\n")
		astr += "\n"
#		astr += (ref[rpos+i:rpos+min(i+fold, len(cstr))] + "\n")
		i += fold
	return(astr)

def extractstring(aln, ref):
	q = []
	r = []

	qi = 0
	ri = aln.pos

	dc = correct_dcigar(aln, ref)
	seq = aln.seq.decode('UTF-8')
	for c in dc:
		if c == 'M' or c == 'X' or c == 'D':
			r.extend(ref[ri])
			ri += 1
		if c == 'M' or c == 'X' or c == 'I' or c == 'N':
			q.extend(seq[qi])
			qi += 1
	return(''.join(q) + '\n' + ''.join(r))

def get_name(qname):
	sname = qname.replace('/', '_').split('_')
#	print(sname)
	name = "%03d_%04d_%04d_%s_%s" % (int(sname[6]), int(sname[1]), int(sname[3]), sname[4][1], sname[16])
#	print(name)
	return name

if __name__ == '__main__':

	ref = []
	with open(sys.argv[2]) as f:
		fa = seq.parse(f, 'fasta')
		for s in fa:
			ref.append(s.seq._data)

	with pysam.Samfile(sys.argv[1], 'r') as b:
		for aln in b.fetch():
#			print(correct_dcigar(aln, ref[0]))
#			correct_cigar(aln, ref)
			name = get_name(aln.qname)
			if len(sys.argv) > 3:
				if name in sys.argv[3:]:
					print(name)
					print(aln.pos)
					print(tostring(aln, ref[0]))
			else:
#				print('> ' + name)
#				print(aln.pos, aln.alen)
#				print(tostring(aln, ref[0]))
				print(extractstring(aln, ref[0]))



